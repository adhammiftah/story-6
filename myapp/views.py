from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import TheForm
from .models import AddStatus

# Create your views here.

def home(request):
	if request.method == 'POST':
		form = TheForm(request.POST)
		if form.is_valid():
			data_item = form.save(commit=False)
			data_item.save()
			return HttpResponseRedirect('/')
	form = TheForm()
	allStatus = AddStatus.objects.all()
	return render(request, 'HelloWorld.html', {'form':form, 'allStatus':allStatus})
