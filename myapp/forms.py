from django.forms import ModelForm
from .models import AddStatus

class TheForm(ModelForm):
    class Meta:
        model = AddStatus
        fields = '__all__'