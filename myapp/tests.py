from django.test import TestCase, Client
from .views import home
from .models import AddStatus
from .forms import TheForm
# Create your tests here.

class Story6UnitTest(TestCase):

	def test_story_6_url_is_exist(self):
		response = Client().get('/')
		self.assertEqual(response.status_code, 200)

	def test_story_6_using_hello_world_template(self):
		response = Client().get('/')
		self.assertTemplateUsed(response, 'HelloWorld.html')

	def test_story_6_landing_page_contains_hello_world(self):
		response = Client().get('/')
		self.assertContains(response, '<h1>Hello, World!</h1>', status_code=200)
    
	def test_model_can_create_new_status(self):
	    # Creating a new activity
	    new_activity = AddStatus.objects.create(title='ppw story add status test')
	
	    # Retrieving all available activity
	    counting_all_available_status = AddStatus.objects.all().count()
	    self.assertEqual(counting_all_available_status, 1)

	def test_story_6_post_success_and_render_the_result(self):
	    test = 'Anonymous'
	    response_post = Client().post('/', {'title': test})
	    self.assertEqual(response_post.status_code, 302)
	
	    response= Client().get('/')
	    html_response = response.content.decode('utf8')
	    self.assertIn(test, html_response)

	def test_story_6_post_error_and_render_the_result(self):
	    test = 'Anonymous'
	    response_post = Client().post('/', {'title': ''})
	    self.assertEqual(response_post.status_code, 200)
	
	    response= Client().get('/')
	    html_response = response.content.decode('utf8')
	    self.assertNotIn(test, html_response)
